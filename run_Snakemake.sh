snakemake -s Snakefile -p --use-conda --configfile config/example.yaml -j --latency-wait 100 --use-singularity --singularity-args '--bind $HOME'
