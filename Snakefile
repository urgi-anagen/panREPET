#############################################
#######      panREPET PIPELINE      #########
#############################################


### VARIABLE DEFINITION & IMPORTS ###
import timeit
import os


start = timeit.timeit()
rootdir = os.getcwd()
out_dir = config.get('output_directory', 'results')

# Tools
TEfinder = config.get('container', {}).get('TE_finder', '')

# Parameters
copies_type = config.get('params', {}).get('copies_type', 'FLC')
extension_length = str( config.get('params', {}).get('extension_length', '') )
cov_flank = config.get('params', {}).get('cov_flank', 80)
cov_consensus = float( config.get('params', {}).get('cov_consensus', 95) )
select_region_bed = config.get('params', {}).get('select_region_bed', None)
selection_type = config.get('params', {}).get('select_type', '')
is_chr_level = config.get('params', {}).get('is_chr_level', False)

if selection_type == "mask":
    selection_type = "-v"

# Data
samples = config.get("GFF", {}).keys()


### FUNCTION DEFINITION ###

#Allows to declare multiple wildcards in the same rule


def getCopiesType(wildcards):
    return f"{out_dir}/extractCopies/{wildcards.sample}_{copies_type}.gff"


def getSubject(wildcards):
    return expand(f"{out_dir}/extendedFasta/{wildcards.db}_extended{extension_length}.fa_format",
                         db = [i for i in config.get('GFF', {}) if i != wildcards.sample ]
                  )

def getExpectedFilenames():
    input_list = []

    db_list= [i for i in config.get('GFF', {})]

    length = extension_length

    for sample in config.get("GFF", {}): #config['GFF']:
        db_list.remove(sample)
        for db in db_list:
            input_list.append(f"{sample}vs{db}_extended{length}")

    return input_list




### RULES ###

rule targets:
    input:
       f"{out_dir}/clique/filter{cov_flank}/filter{cov_flank}.stats",
       f"{out_dir}/clique/filter{cov_flank}/filter{cov_flank}_consensusUnique.txt",
       f"{out_dir}/uniqueCopies/filter{cov_flank}/concat_uniqueCopies_filter{cov_flank}.tsv"




rule extractCopies:
    input:
       gff = lambda wildcards : config.get('GFF', {}).get(wildcards.sample, ''),
       consensus = config.get('Consensus', '')

    output:
         FLF = "{out_dir}/extractCopies/{sample}_FLF_wholegenome.gff",
         FLC = "{out_dir}/extractCopies/{sample}_FLC_wholegenome.gff",
         other = "{out_dir}/extractCopies/{sample}_other_wholegenome.gff",
         all = "{out_dir}/extractCopies/{sample}_all_wholegenome.gff"

    #conda:
    #    "envs/biopython.yaml"

    log:
      "{out_dir}/log/extractCopies_{sample}.log"

    params:
      covCons = cov_consensus

    script:
        "./scripts/extractCopies.py"




rule selectOrMaskRegion:
    input:
         FLF = "{out_dir}/extractCopies/{sample}_FLF_wholegenome.gff",
         FLC = "{out_dir}/extractCopies/{sample}_FLC_wholegenome.gff",
         other = "{out_dir}/extractCopies/{sample}_other_wholegenome.gff",
         all = "{out_dir}/extractCopies/{sample}_all_wholegenome.gff"

    output:
         FLF = "{out_dir}/extractCopies/{sample}_FLF.gff",
         FLC = "{out_dir}/extractCopies/{sample}_FLC.gff",
         other = "{out_dir}/extractCopies/{sample}_other.gff",
         all = "{out_dir}/extractCopies/{sample}_all.gff"

    params:
         select_region = "{out_dir}/extractCopies/{sample}_selected_or_masked_regions.bed",
         selection_type = selection_type

    shell: """
                if [ -e {select_region_bed} ]; then
                    grep {wildcards.sample} {select_region_bed} | cut -f 1,2,3 > {params.select_region}
                    bedtools intersect {params.selection_type} -a {input.FLF} -b {params.select_region} > {output.FLF}
                    bedtools intersect {params.selection_type} -a {input.FLC} -b {params.select_region} > {output.FLC};
                    bedtools intersect {params.selection_type} -a {input.other} -b {params.select_region} > {output.other};
                    bedtools intersect {params.selection_type} -a {input.all} -b {params.select_region} > {output.all}
                else
                    cp {input.FLF} {output.FLF}
                    cp {input.FLC} {output.FLC}
                    cp {input.other} {output.other}
                    cp {input.all} {output.all}
                fi
         """




rule getChromSize:
    input:
         lambda wildcards: config.get('Ref', {}).get(wildcards.sample, '')

    output:
         "{out_dir}/extendedGFF/{sample}.genome"

    log:
         "{out_dir}/log/getChromSize_{sample}.log"

    script:
         "./scripts/fastaLength.py"




rule extendGFF:
    input:
        gff = getCopiesType, #"{out_dir}/extractCopies/{sample}_FLC.gff",
        genome_file = "{out_dir}/extendedGFF/{sample}.genome"

    output:
        "{out_dir}/extendedGFF/{sample}_extended{length}.gff"

    log:
        "{out_dir}/log/extendGFF_{sample}_extended{length}.log"

    params:
        len_ext = extension_length

    shell:
        "bedtools slop -i {input.gff} -g {input.genome_file} -b {params.len_ext} > {output}"




#Gets sequence in FASTA format from GFF coordinates
rule getFastaFromGFF:
    input:
        ref = lambda wildcards: config["Ref"][wildcards.sample],
        gff_ext = "{out_dir}/extendedGFF/{sample}_extended{length}.gff"

    output:
        unprocessed = "{out_dir}/extendedFasta/{sample}_extended{length}.fa"

    log:
      "{out_dir}/log/getFastaFromGFF_{sample}_extended{length}.log"

    shell:
        "bedtools getfasta -fi {input.ref} -bed {input.gff_ext} -fo {output.unprocessed} &> {log}"




rule reformatFasta:
    input:
        "{out_dir}/extendedGFF/{sample}_extended{length}.gff",
        "{out_dir}/extendedFasta/{sample}_extended{length}.fa"

    output:
        "{out_dir}/extendedFasta/{sample}_extended{length}.fa_format"

    #conda:
    #    "envs/biopython.yaml"

    log:
        "{out_dir}/log/reformatFasta_{sample}_extended{length}.log"

    params:
        "{sample}"

    script:
        "./scripts/reformat.py"




rule minimap2:
    input:
        query = "{out_dir}/extendedFasta/{sample}_extended{length}.fa_format",
        subject = getSubject

    output:
        "{out_dir}/minimap2/{sample}_extended{length}/{sample}vs{db}.minimap2"

    #conda:
    #    "envs/minimap2.yaml"

    log:
      "{out_dir}/log/minimap2{sample}vs{db}_{length}.log"

    params:
        outdir = out_dir,
        output_subdir = "{out_dir}/minimap2/{sample}_extended{length}/{sample}vs{db}",
        minimap2 =  '-x asm20',
        sample = lambda wildcards : wildcards.sample,
        db = lambda wildcards : wildcards.db,
        extension_length = extension_length,
        is_chr_level = is_chr_level

    benchmark:
        "{out_dir}/benchmarks/{sample}vs{db}_extended{length}.minimap2.benchmark.txt"

    threads: 1

    #priority: 5

    script:
        "./scripts/launchMinimap2.py"




rule minimap2align:
    input:
        "{out_dir}/minimap2/{sample}_extended{length}/{sample}vs{db}.minimap2"

    output:
        "{out_dir}/minimap2/{sample}_extended{length}/{sample}vs{db}.minimap2.align"

    log:
      "{out_dir}/log/minimap2align_{sample}vs{db}_{length}.log"

    benchmark:
        "{out_dir}/benchmarks/{sample}vs{db}_extended{length}.minimap2align.benchmark.txt"

    #priority: 10

    shell:
        "python3 scripts/minimap2align.py {input} &> {log}"




rule matcher:
    input:
        align = rules.minimap2align.output

    output:
        bed = "{out_dir}/matcher/{sample}_extended{length}/{sample}vs{db}.clean_match.bed",
        others = temp(expand("{{out_dir}}/matcher/{{sample}}_extended{{length}}/{{sample}}vs{{db}}.clean_match.{extension}", extension = ["param", "path", "gff3"])) #["map", "param", "path", "path.attr", "gff3"]

    singularity:
        TEfinder

    log:
      "{out_dir}/log/matcher_{sample}vs{db}_{length}.log"

    params:
        output_matcher = "{out_dir}/matcher/{sample}_extended{length}/{sample}vs{db}"

    benchmark:
        "{out_dir}/benchmarks/{sample}vs{db}_extended{length}.matcher.benchmark.txt"

    threads: 1

    #priority: 20

    shell:
        '''
	matcherThreads2.31 -m {input.align} -B {params.output_matcher} -j -x -v 1 -t 1 &> {log} ;
	touch {output.bed} &> {log} ; # The blaster output can be empty
	rm {log}
        '''




rule bestHit:
    input:
        access1 = "{out_dir}/matcher/{sample}_extended{length}/{sample}vs{db}.clean_match.bed",
        access2 = "{out_dir}/matcher/{db}_extended{length}/{db}vs{sample}.clean_match.bed"

    output:
        "{out_dir}/bestHits/filter{cov_flank}/{sample}vs{db}_extended{length}_filter{cov_flank}.txt"

    log:
        "{out_dir}/log/bestHit_{sample}vs{db}_extended{length}_filter{cov_flank}.log"

    params:
          cov_flank = cov_flank,
          cov_match = lambda wildcards :  config['params']['cov_match'],
          len_extension = extension_length

    benchmark:
        "{out_dir}/benchmarks/{sample}vs{db}_extended{length}_filter{cov_flank}.bestHits.benchmark.txt"


    #priority: 30

    shell:
        "python3 scripts/bestHits.py -fi {input.access1} "
                                          "-si {input.access2} "
                                          "-cf {params.cov_flank} "
                                          "-cm {params.cov_match} "
                                          "-e {params.len_extension} "
                                          "-o {output} &> {log}"




rule catBestHits:
    input:
        expand('{{out_dir}}/bestHits/filter{{cov_flank}}/{filename}_filter{{cov_flank}}.txt', filename = getExpectedFilenames())

    output:
        "{out_dir}/bestHits/filter{cov_flank}/concat_filter{cov_flank}.txt"

    log:
        "{out_dir}/log/catBestHits_filter{cov_flank}.log"

    params:
        "{out_dir}/bestHits/filter{cov_flank}/*"

    shell:
        "cat {input} >> {output}"




rule clique:
    input:
         "{out_dir}/bestHits/filter{cov_flank}/concat_filter{cov_flank}.txt"

    output:
        cliques_stats = "{out_dir}/clique/filter{cov_flank}/cliques_stats_filter{cov_flank}.tsv",
        cliques_copy = "{out_dir}/clique/filter{cov_flank}/cliques_copy_filter{cov_flank}.tsv"

    #conda:
    #    "envs/networkX.yaml"

    log:
        "{out_dir}/log/clique_filter{cov_flank}.log"

    params:
        accessions = expand( config.get('GFF', {}) )

    benchmark:
        "{out_dir}/benchmarks/filter{cov_flank}.cliques.benchmark.txt"

    shell:
        "./scripts/cliques.py -i {input} "
                                     "-s {output.cliques_stats} "
                                     "-c {output.cliques_copy} "
                                     "-a {params.accessions} &> {log}"




rule uniqueCopies:
    input:
        cliquesFile = "{out_dir}/clique/filter{cov_flank}/cliques_copy_filter{cov_flank}.tsv",
        gffExtFile = f"{{out_dir}}/extendedGFF/{{sample}}_extended{extension_length}.gff"

    output:
        "{out_dir}/uniqueCopies/filter{cov_flank}/{sample}_uniqueCopies_filter{cov_flank}.tsv"

    log:
        "{out_dir}/log/uniqueCopies_filter{cov_flank}_{sample}.log"

    benchmark:
        "{out_dir}/benchmarks/filter{cov_flank}_{sample}.uniqueCopies.benchmark.txt"

    shell:
        "./scripts/uniqueCopies.py {wildcards.sample} {input.cliquesFile} {input.gffExtFile} {output} &> {log}"




rule catUniCopie:
    input:
        expand('{{out_dir}}/uniqueCopies/filter{{cov_flank}}/{fileSample}_uniqueCopies_filter{{cov_flank}}.tsv', fileSample = config.get('GFF', ''))

    output:
        "{out_dir}/uniqueCopies/filter{cov_flank}/concat_uniqueCopies_filter{cov_flank}.tsv"

    log:
        "{out_dir}/log/catUniqueCopies_filter{cov_flank}.log"

    shell: "cat {input} >> {output}"




rule statistics:
    input:
        cliques_stats = "{out_dir}/clique/filter{cov_flank}/cliques_stats_filter{cov_flank}.tsv",
        cliques_copies = "{out_dir}/clique/filter{cov_flank}/cliques_copy_filter{cov_flank}.tsv",
        concat_uniqueCopies = "{out_dir}/uniqueCopies/filter{cov_flank}/concat_uniqueCopies_filter{cov_flank}.tsv"

    output:
        consensusUnique = "{out_dir}/clique/filter{cov_flank}/filter{cov_flank}_consensusUnique.txt",
        final_stats = "{out_dir}/clique/filter{cov_flank}/filter{cov_flank}.stats"

    #log:
        #"{out_dir}/log/statistics_filter{cov_flank}_{length}.log"

    shell:
        "./scripts/stats.py {input.cliques_stats} {input.cliques_copies} {input.concat_uniqueCopies} {output.consensusUnique} {output.final_stats}"



stop = timeit.timeit()
