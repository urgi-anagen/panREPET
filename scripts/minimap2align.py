#!/usr/bin/env python

import sys


input_file = sys.argv[1]


minimap2_file = open(input_file)
align_file = open(input_file+".align","w")
for l in minimap2_file:
    tok=l.split('\t')
    qname = tok[0]
    if tok[4]=="+":
        qstart = int(tok[2])+1
        qend = int(tok[3])+1
    else:
        qstart = int(tok[3])+1
        qend = int(tok[2])+1
    sname = tok[5]
    sstart = int(tok[7])+1
    send = int(tok[8])+1
    score = tok[9]
    identity = (float(tok[9])/int(tok[10]))*100
    eval = 0.0
    output = "{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(qname,qstart,qend,sname,sstart,send,eval,score,identity)
    align_file.write(output)

minimap2_file.close()
align_file.close()
