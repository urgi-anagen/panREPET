#!/usr/bin/env python
import re
import sys

accession_name = sys.argv[1] # Extract accession name
cliques_copy = sys.argv[2]
all_copies = sys.argv[3] # gff file
copies_uniques = sys.argv[4] # output: gff file with accession name at the end

with open(cliques_copy, 'r') as clq:
    next(clq)
    copyCliqAcc = {}
    for line in clq:
        idacc = line.split()[1]
        idcopie = line.split()[5]
        copyCliqAcc[(idcopie, idacc)] = idacc

with open(all_copies, 'r') as allcp:
    with open(copies_uniques, 'w') as uniqCopy_file:
        for line in allcp:

            copyAllId = re.search("ID=(.*?)\;", line).group(1)
            if "Name=" in line:
                copyAllId_name = re.search("Name=(.*?)\;", line).group(1)
                copyAllId+= "_" + copyAllId_name

            accAll = accession_name
            if copyCliqAcc.get((copyAllId, accAll)) is None:
                line = line.rstrip('\n')
                uniqCopy_file.write(f"{line}\t{accession_name}\n")
