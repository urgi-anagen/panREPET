#!/usr/bin/env python

import os
import shutil
import subprocess
import sys
from Bio import SeqIO

#inputs
query = snakemake.input[0]
subject = snakemake.input[1]

#ouput
output = snakemake.output[0]

#params
out_dir = snakemake.params[0]
pathDir = snakemake.params[1]
params = snakemake.params[2]
sample = snakemake.params[3]
db = snakemake.params[4]
length = snakemake.params[5]
is_chr_level = snakemake.params[6]

if os.path.exists(pathDir):
    shutil.rmtree(pathDir)

os.mkdir(pathDir)
os.chdir(pathDir)

dst = os.getcwd()

fileQuery = f"{sample}_extended{length}.fa_format"
fileQuery = os.path.join(dst, fileQuery)
fileSubject = f"{db}_extended{length}.fa_format"
fileSubject = os.path.join(dst, fileSubject)

os.symlink(query, fileQuery)
shutil.copyfile(subject, fileSubject)

# For each sequence of query file,
# create a folder for each sequence (i.e. a TE copy)
f_open = open(fileQuery, "r")

for rec in SeqIO.parse(f_open, "fasta"):
   id_query = rec.id
   seq_query = rec.seq
   id_query_cutting = id_query.split("/")
   chromosome_query = id_query_cutting[1]
   copy_name = id_query_cutting[2]
   consensus_name_query = id_query_cutting[3]
   os.mkdir(copy_name)
   os.chdir(copy_name)
   id_file = open(copy_name + ".fa", "w")
   id_file.write(">"+str(id_query)+"\n"+str(seq_query))
   id_file.close()

   ## Keep only subject sequences with same consensus that the current query sequence
   # and same chromosome
   shutil.copyfile(fileSubject,  os.path.basename(fileSubject))
   with open(os.path.basename(fileSubject + ".clean"), "w") as f_clean:
       for subject in SeqIO.parse(fileSubject, "fasta"):
           id_subject = subject.id
           id_subject_cutting = id_subject.split("/")
           chromosome_subject = id_subject_cutting[1]
           consensus_name_subject = id_subject_cutting[3]
           seq_subject = subject.seq
           if consensus_name_subject == consensus_name_query:
               if is_chr_level == True:
                   if chromosome_subject == chromosome_query:
                       f_clean.write(">"+str(id_subject)+"\n"+str(seq_subject)+"\n")
               else:
                   f_clean.write(">"+str(id_subject)+"\n"+str(seq_subject)+"\n")
                       

   query = copy_name + ".fa"
   subject = os.path.basename(fileSubject) + ".clean"
   output_copy = copy_name + ".out"
   cmd = f"touch 'tmp.log' ; minimap2 {subject} {query} {params} > {output_copy} 2> {'tmp.log'}"
   #print("\nCmd:", cmd, "\n")
   subprocess.call(cmd, shell = True)
   # Copy the align output to the proper directory
   if os.path.exists(copy_name + ".out"):
      shutil.copyfile(copy_name + ".out", "{}/{}".format("..", copy_name + ".out"))

   # Change the working directory and remove the created temporal directory
   os.chdir("../")
   shutil.rmtree(copy_name)

cmd = f"cat *.out > {output}"
subprocess.call(cmd, shell = True)
os.chdir("../")
shutil.rmtree("{}vs{}".format(sample, db))

f_open.close()
