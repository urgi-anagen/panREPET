#!/usr/bin/env python

from Bio import SeqIO
import re

accession = snakemake.params[0]


# Open the gff file and stores in a list the identification data for each copy
with open(snakemake.input[0], 'r') as gff:
    header_list = []
    for line in gff:
        chrom = line.split("\t")[0]
        #gff_id = re.search("ID=(.+?)_.+Target=(.+?)\s", line)
        #copy = (gff_id.group(1))
        #consensus = gff_id.group(2)

        other_features = line.split("\t")[8]
        copy = re.search("ID=(.*?)\;", other_features).group(1)
        if "Name=" in other_features:
                copy_name = re.search("Name=(.*?)\;", other_features).group(1)
                copy+= "_" + copy_name
        consensus = re.search("Target=(.*?)\ ", other_features).group(1)

        header_list.append(f"{chrom}/{copy}/{consensus}")


# Open the sequences file and reformat it in fasta60 format while updating headers
with open(snakemake.input[1], 'r') as file:
    with open(snakemake.output[0], "w") as reformated:
        header_iterator = 0
        for record in SeqIO.parse(file, "fasta-2line"):
            pos = re.search("^.+:(.+)", record.id).group(1)
            record.description = f"{accession}/{ header_list[header_iterator]}/{pos}"
            record.id = f"{accession}/{ header_list[header_iterator]}/{pos}"
            SeqIO.write(record, reformated, "fasta")
            header_iterator += 1
