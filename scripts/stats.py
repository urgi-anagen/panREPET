#!/usr/bin/env python
import sys, statistics, re, math
import pandas as pd

# inputs
cliques_stats = sys.argv[1]
cliques_copies = sys.argv[2]
concat_unique_copies = sys.argv[3]

# outputs
consensus_unique = sys.argv[4]
stats_output = sys.argv[5]


####################################################################################################################
# Statistics about cliques

df = pd.read_csv(cliques_stats, sep="\t")

nb_clq = len(df)
nb_accs = df["clique_size"].max()
min_soft_core = math.trunc(nb_accs * 0.95)
min_shell = math.trunc(nb_accs * 0.2)

nb_core_clq = len(df[df["pangenomic_compartment"] == 'core'])
nb_softcore_clq = len(df[df["pangenomic_compartment"] == 'soft_core'])
nb_shell_clq = len(df[df["pangenomic_compartment"] == 'shell'])
nb_cloud_clq = len(df[df["pangenomic_compartment"] == 'cloud'])

df_nbCliquesPerSize = df.groupby("id").agg({"clique_size": "first"}).reset_index()
df_nbCliquesPerSize = df_nbCliquesPerSize.groupby("clique_size").count().reset_index()
df_nbCliquesPerSize.columns = ["clique_size", "nb_cliques"]


####################################################################################################################
# Statistics about copies

with open(cliques_copies, 'r') as clq_copies:
    next(clq_copies)
    nb_copy = 0
    nb_copy_per_accession = {}
    heterogen = {}
    list_length = []

    for line in clq_copies:
        nb_copy += 1
        acc_name = line.split()[1]
        list_length.append(int(line.split()[4]) - int(line.split()[3]))

        if acc_name not in nb_copy_per_accession:
            nb_copy_per_accession[acc_name] = 1
        else:
            nb_copy_per_accession[acc_name] += 1

        nb_copy_per_accession = dict(sorted(nb_copy_per_accession.items(), key=lambda x: x[0]))

clq_copies.close()


####################################################################################################################
# Statistics about unique copies

with open(concat_unique_copies, 'r') as unique_copyInfo:
    with open(consensus_unique, "w") as consensus_unique:

        nb_uniCopy = 0
        nb_uniCopy_per_accession = {}
        nb_consperAcc = {}
        consAcc = {}

        for line in unique_copyInfo:

            nb_uniCopy+=1
            accessions = line.split('\t')[9].rstrip("\n")

            #idconsensus = line.split('\t')[8].rsplit("=")[2]
            idconsensus = re.search("ID=(.*?)\;", line).group(1)
            if "Name=" in line:
                idconsensus_Name = re.search("Name=(.*?)\;", line).group(1)
                idconsensus+= "_" + idconsensus_Name

            consAcc[(idconsensus, accessions)] = accessions
            
            if accessions not in nb_uniCopy_per_accession:
                nb_uniCopy_per_accession[accessions] = 1
            else:
                nb_uniCopy_per_accession[accessions] += 1

            nb_uniCopy_per_accession = dict(sorted(nb_uniCopy_per_accession.items(), key=lambda x: x[0]))

            if (idconsensus, accessions) not in nb_consperAcc:
                nb_consperAcc[idconsensus, accessions] = 1
            else:
                nb_consperAcc[idconsensus, accessions] += 1

            nb_consperAccSort = {k: v for k, v in sorted(nb_consperAcc.items(), key=lambda item: item[1], reverse=True)}


        for (key, value) in nb_consperAccSort.items():
            idconsensus, accessions = key
            consensus_unique.write(f"{accessions}\t{idconsensus}\t{value}\n")


####################################################################################################################
# Write on output

nb_clq_and_unique = nb_clq + nb_uniCopy

with open(stats_output, 'w') as stats_output:
    stats_output.write(f"Number of shared TE copies: {nb_copy}\n"
		   f"Number of cliques (i.e. shared TE insertions): {nb_clq}\n"
		   f"Unique copies: {nb_uniCopy} ({round(nb_uniCopy/nb_clq_and_unique*100, 2)}%)\n\n"

		   f"Number of core (all accs, {nb_accs} accs) cliques: {nb_core_clq} ({round(nb_core_clq/nb_clq_and_unique*100, 2)}%)\n"
		   f"Soft-core (95-98% of accs, {min_soft_core}-{nb_accs-1} accs): {nb_softcore_clq} ({round(nb_softcore_clq/nb_clq_and_unique*100, 2)}%)\n"
		   f"Shell (20-94%, {min_shell}-{min_soft_core-1}): {nb_shell_clq} ({round(nb_shell_clq/nb_clq_and_unique*100, 2)}%)\n"
		   f"Cloud (2-20%, 2-{min_shell-1}): {nb_cloud_clq} ({round(nb_cloud_clq/nb_clq_and_unique*100, 2)}%)\n\n"

		   f"Number of cliques per clique size : \n{df_nbCliquesPerSize}\n\n"

		   f"Number of copies (core and dispensable) per accession = {nb_copy_per_accession}\n"
		   f"Number of unique copies = {nb_uniCopy_per_accession}\n\n"

		   f"Copies lengths (core and dispensable):\n"
		   f"min = {min(list_length)}\nmax = {max(list_length)}\nmedian = {round(statistics.median(list_length), 2)}\n"
		   f"mean = {round(statistics.mean(list_length), 2)}"
		   )
    stats_output.close()
