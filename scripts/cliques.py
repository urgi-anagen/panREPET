#!/usr/bin/env python

import logging
import argparse
import re
import networkx as nx
import math


class Cliques(object):

    def __init__(self, filename="", filename2="", bestHit="", ac = []):
        self.filename = filename
        self.filename2 = filename2
        self.bestHit = bestHit
        self.ac = ac

    def setAttributesFromCmdLine(self):
        parser = argparse.ArgumentParser(usage="cliques.py [options]",
                                         description="",
                                         epilog="")

        parser.add_argument('-i','--input', help= 'first input file', required= True, metavar='file')
        parser.add_argument('-c', '--copy_output', help='coreET output file', required=True, metavar='file')
        parser.add_argument('-s', '--stats_output', help='dispensableET output file', required=True)
        parser.add_argument('-a', '--list_of_accessions', nargs="*", help='', required=True, type=str)
        args=parser.parse_args()
        self.setAttributesFromArgs(args)

    def setAttributesFromArgs(self, args):
        self.filename = args.stats_output
        self.filename2 = args.copy_output
        self.bestHit = args.input
        self.ac = args.list_of_accessions

    logging.basicConfig(format='%(levelname)s - %(asctime)s - %(message)s', level=logging.INFO)
    logging.info("Cliques is running")

    def getCliques(self, input_file):
        graph = nx.read_edgelist(input_file)

        cliques = nx.clique.find_cliques(graph)

        return cliques

    def isCore(self, clique, nb_accessions):
        nb_accessions = len(nb_accessions)
        if len(clique) == nb_accessions:
            return 'core'
        else:
            return 'dispensable'

    def isPanComp(self, clique, nb_accessions):
        nb_accessions = len(nb_accessions)
        min_soft_core = math.trunc(nb_accessions * 0.95)
        min_shell = math.trunc(nb_accessions * 0.2)
        if len(clique) == nb_accessions:
            return 'core'
        if min_soft_core <= len(clique) < nb_accessions:
            return 'soft_core'
        if min_shell <= len(clique) < min_soft_core:
            return 'shell'
        if 1 < len(clique) < min_shell:
            return 'cloud'

    def getCliquesAccessions(self, clique):
        accessions = []
        for i in clique:
            accessions.append(i.split("/")[0])

        return accessions

    def run(self):
        with open(self.filename, 'w') as stats_cliques:
            stats_cliques.write(f"id\tclique_size\tcore/dispensable\tpangenomic_compartment\taccessions\tclique\n")
            with open(self.filename2, 'w') as tab_cliques:
                tab_cliques.write(
                    f"id\taccession\tchromosome\tstart\tend\tcopy_name\tconsensus\tclique_size\tpangenomic_compartment\n")
                clik = self.getCliques(self.bestHit)
                id = 0
                for clq in clik:
                    id +=1
                    size = len(clq)
                    status = self.isCore(clq, self.ac)
                    status2 = self.isPanComp(clq, self.ac)
                    miss = self.getCliquesAccessions(clq)
                    stats_cliques.write(f"{id}\t{size}\t{status}\t{status2}\t{miss}\t{clq}\n")
                    for i in clq:
                        accession = i.split("/")[0]
                        copy_name = i.split("/")[2]
                        chromosome = i.split("/")[1]
                        consensus = i.split("/")[3]
                        pos = re.search("(\d+)-(\d+)", i.split("/")[4])
                        start = pos.group(1)
                        end = pos.group(2)

                        tab_cliques.write(
                            f"{id}\t{accession}\t{chromosome}\t{start}\t{end}\t{copy_name}\t{consensus}\t{size}\t{status2}\n")


if __name__ == '__main__':
    iC = Cliques()
    iC.setAttributesFromCmdLine()
    iC.run()
