#!/usr/bin/env python

import re
import argparse
import logging
import os


class BestBijectiveHits(object):

    def __init__(self, tabFileAccession1="", tabFileAccession2="", outputFile=""):
        self.tabFileAccession1 = tabFileAccession1
        self.tabFileAccession2 = tabFileAccession2
        self.outputFile = outputFile

    def setAttributesFromCmdLine(self):
        parser = argparse.ArgumentParser(usage="bestHits.py [options]",
                                         description="Script analyzing matcher results and giving best bijective pairs",
                                         epilog="z")

        parser.add_argument('-fi', '--first_input', help= 'first input file', required= True, metavar='file')
        parser.add_argument('-si', '--second_input', help='second input file', required=True, metavar='file')
        parser.add_argument('-o', '--output', help='output file', required=True)
        parser.add_argument('-cf', '--coverageFlank', help='', required=True, type=float)
        parser.add_argument('-cm', '--coverageMatch', help='', required=True, type=float)
        parser.add_argument('-e', '--extension', type = int)
        #parser.add_argument('-v', '--verbosity', help='verbosity', required=True, metavar='file')
        args=parser.parse_args()
        self._setAttributesFromArguments(args)

    def _setAttributesFromArguments(self, args):
        self.tabFile1 = args.first_input
        self.tabFile2 = args.second_input
        self.output = args.output
        self.cov_f = int(args.coverageFlank) / 100
        self.cov_m = args.coverageMatch
        self.lenExt = args.extension

    #logging.getLogger().setLevel(logging.INFO)
    logging.basicConfig(format='%(levelname)s - %(asctime)s - %(message)s', level=logging.INFO)
    logging.info("BBH is running.")

    def getBestHit(self, input_file):

        if not os.path.exists(input_file):
            logging.warning(f"{input_file} input file doesn't exist.")

        with open(input_file, 'r') as bed:
            next(bed)
            hits = {}
            for line in bed:
                
                cutting = line.split()
                qname = cutting[0]
                sname = cutting[3]
                qstart = int(cutting[1])
                qstop = int(cutting[2])
                blast_score = cutting[4]
                #length = (int(re.search("(\d+)-(\d+)", qname).group(2))-int(re.search("(\d+)-(\d+)", qname).group(1)))
                qname_cutting = qname.split("/")[4].split('-')
                length = int(qname_cutting[1]) - int(qname_cutting[0])
                match_cov = (qstop - qstart +1) / length

                if qname not in hits:
                    hits[qname] = (sname, blast_score, match_cov, qstart, qstop, length)

                elif float(blast_score) > float(hits[qname][1]):
                    hits[qname] = (sname, blast_score, match_cov, qstart, qstop, length)

        return hits

    def getBijective(self, first_hits, second_hits, cov_f, cov_m):
        hit_pairs = []
        cov_f = float(cov_f)
        cov_m = float(cov_m)
        for i in first_hits:
            if self.checkFlankCov(first_hits, i, cov_f, cov_m, self.lenExt):
                hit = first_hits[i][0]
                if hit in second_hits.keys():
                    if self.checkFlankCov(second_hits, hit, cov_f, cov_m, self.lenExt) and second_hits[hit][0] == i:
                        hit_pairs.append((i, hit))

        return hit_pairs

    def checkFlankCov(self, hit_dict, copy, cov_flank_param, cov_match_param, lenExt):
        pos_start = hit_dict[copy][3]
        pos_stop = hit_dict[copy][4]
        cov_match = hit_dict[copy][2]
        length_ext_copy = hit_dict[copy][5]

        left_flank_cov = (lenExt - pos_start)/lenExt
        right_flank_cov = 1-((length_ext_copy - pos_stop)/lenExt)

        if float(cov_match) >= cov_match_param:
            if left_flank_cov >= cov_flank_param and right_flank_cov >= cov_flank_param: ##float(cov_match) >= 0.0 and
                return True

    def writeOutput(self, hit_pairs, output):

        with open(output, 'w') as output_file:
            for i in hit_pairs:
                output_file.write(i[0] + '\t' + i[1] + '\n')

    def run(self):
        first_hits = self.getBestHit(self.tabFile1)
        second_hits = self.getBestHit(self.tabFile2)
        hit_pair = self.getBijective(first_hits, second_hits, self.cov_f, self.cov_m)

        if hit_pair:
            self.writeOutput(hit_pair, self.output)


if __name__ == '__main__':
    iBBH = BestBijectiveHits()
    iBBH.setAttributesFromCmdLine()
    iBBH.run()

    logging.info("BBH is finished.")
