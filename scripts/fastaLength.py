#!/usr/bin/env python

import sys, os

class Bioseq:

   def __init__(self):
      self.header=""
      self.sequence=""

   def read(self,file):
      if file:
         line=file.readline()
         if line == "":
            self.header = None
            self.sequence = None
            return
         if line[0]=='>':
            self.header=line.lstrip('>').rstrip()
         else:
            print("error, line is",line.rstrip())
            return
         seq=""
      while file:
         prev_pos=file.tell()
         line=file.readline()
         if line=="":
            break
         if line[0]=='>':
            file.seek(prev_pos)
            break
         seq+=line.rstrip()
      self.sequence=seq
      return

   def load(self,filename):
      file=open(filename)
      self.read(file)

   def save(self,filename):
      file=open(filename,"w")
      self.write(file)

   def write(self,file):
      file.write('>'+self.header+'\n')
      i=0
      while i<len(self.sequence):
         file.write(self.sequence[i:i+60]+'\n')
         i=i+60

   def subseq(self,s,e=0):
      if e==0 :
         e=len(self.sequence)
      if s>e :
         print("error: start must be < or = to end")
         return
      if s<=0 :
         print("error: start must be > 0")
         return
      sub=Bioseq()
      sub.header=self.header+" fragment "+str(s)+".."+str(e)
      sub.sequence=self.sequence[(s-1):e]
      return sub

   def length(self):
      return len(self.sequence)

#------------------------------------------------------------------------------------------------------------
# Get total size in nucleotide of a fasta file and the count per sequence in a file *.len
def fastaLength(fasta_filename):
   file_db = open(fasta_filename)
   file_len= open(snakemake.output[0],"w")
   seq = Bioseq()
   total_length = 0
   while 1:
      seq.read(file_db)
      if seq.sequence == None:
         break
      l = seq.length()
      total_length += l
      file_len.write('{}\t{}\n'.format(seq.header, l))
   file_len.close()
   file_db.close()

   return total_length

fastaLength(snakemake.input[0])