#!/usr/bin/env python

import re
from seqIdLenFasta import Bioseq, fastaLength

#inputs
gff_file = snakemake.input[0]
consensus_file = snakemake.input[1]

#outputs
flf_file = snakemake.output[0]
flc_file = snakemake.output[1]
other_file = snakemake.output[2]
all_file = snakemake.output[3]

#params
cov_cons = float(snakemake.params[0])


with open(gff_file, 'r') as gff:
    with open(flf_file, 'w') as flf:
        with open(flc_file, 'w') as flc:
            with open(other_file, 'w') as other, open(all_file, 'w') as all:
                conSize = fastaLength(consensus_file)

                for line in gff:
                    if line.split()[2] == 'match':
                        start = line.split()[3]
                        stop = line.split()[4]
                        next_line = next(gff)
                        target = re.search(r'Target=(.*?) ', line.split("\t")[8]).group(1)
                        target_length = conSize[target]

                        len_match = float(stop) - float(start)

                        if cov_cons <= len_match / target_length <= (1+(1-cov_cons)) :
                            if next_line.split()[4] == stop and next_line.split()[3] == start:
                                flf.write(line)
                                flc.write(line)
                            else:
                                len_match_fragments = 0
                                start1 = next_line.split()[3]
                                stop1 = next_line.split()[4]
                                len_match_fragments+= float(stop1) - float(start1)
                                while next_line.split()[3] != start:
                                    start1 = next_line.split()[3]
                                    stop1 = next_line.split()[4]
                                    len_match_fragments+= float(stop1) - float(start1)
                                    next_line = next(gff)
                                if cov_cons <= len_match_fragments / target_length <= (1+(1-cov_cons)) :
                                    flc.write(line)
                                else:
                                    other.write(line)
                        else:
                            other.write(line)

                        all.write(line)
